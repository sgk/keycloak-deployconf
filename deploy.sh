#!/bin/bash 

# creating a pvc
oc create -f storage0pv.yaml

# deploying postgresql
oc new-app -f postgresql.json
sleep 20

# deploying keycloak
oc new-app -f keycloak.json
