## Keycloak Deploy scripts

### how to deploy

Create a new project on openshift

`$ oc new-project keycloak --display-name="Keycloak server" --description="keycloak server + postgres"`

clone this repo

`git clone https://gitlab.com/sgk/keycloak-deployconf.git`

and run the deploy.sh

`$ bash deploy.sh`

### For customising:

#### keycloak

edit environment variables:

                "env":[
                  {
                    "name":"KEYCLOAK_USER",
                    "value":"admin"
                  },
                  {
                    "name":"KEYCLOAK_PASSWORD",
                    "value":"admin"
                  },
                  {
                    "name":"POSTGRES_DATABASE",
                    "value":"userdb"
                  },
                  {
                    "name":"POSTGRES_USER",
                    "value":"keycloak"
                  },
                  {
                    "name":"POSTGRES_PASSWORD",
                    "value":"password"
                  },
                  {
                    "name":"POSTGRES_PORT_5432_TCP_ADDR",
                    "value":"postgres"
                  },
                  {
                    "name":"POSTGRES_PORT_5432_TCP_PORT",
                    "value":"5432"
                  }
                ]


#### postgresql 

edit environment variables:

            "env": [
              {
                "name": "POSTGRESQL_USER",
                "value": "keycloak"
              },
              {
                "name": "POSTGRESQL_PASSWORD",
                "value": "password"
              },
              {
                "name": "POSTGRESQL_DATABASE",
                "value": "userdb"
              },
              {
                "name": "POSTGRESQL_ADMIN_PASSWORD",
                "value": "password"
              }
            ],

edit volume claim:

        "volumes": [
          {
            "name": "pgdata",
            "persistentVolumeClaim": {
              "claimName": "storage0"
            }
          }
        ]

